﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend_Task2.ViewModels;
using Backend_Task2.Forum.Models;
using Backend_Task2.Forum;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Backend_Task3.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Backend_Task2.Controllers
{
    [Authorize]
    public class MessageController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IMessageService _service;
        private readonly UserManager<AppUserModel> _userManager;


        public MessageController(ApplicationDbContext context, IMessageService service, UserManager<AppUserModel> userManager)
        {
            _context = context;
            _service = service;
            _userManager = userManager;
        }

        // GET: MessageController/Create
        public ActionResult Create(int id)
        {
            return View(new MessageCreateViewModel { TopicID = id});
        }

        // POST: MessageController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(MessageCreateViewModel newMessage)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(newMessage);
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new UnauthorizedAccessException($"User with the name {User.Identity.Name} was not found");
                }

                newMessage.Created = DateTime.Now;

                var topicID = -1;

                if (TempData["ForumID"] != null)
                {
                    topicID = (int)TempData["TopicID"];
                }

                await _service.CreateAsync(newMessage, user);

                TempData["SuccessMessage"] = "Сообщение успешно отправлено.";

                if (topicID > -1)
                {
                    return RedirectToAction("Details", "Topic", new { id = topicID });
                }
                else
                {
                    return RedirectToAction("Index", "Forum");
                }
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return View(newMessage);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(newMessage);
            }
        }

        // GET: MessageController/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            var messageEditView = await _service.GetForEditAsync(id);


            if (messageEditView is null)
            {
                TempData["ErrorMessage"] = "Неправильный запрос. Сообщение не существует или уже удалено.";
                return RedirectToAction("Index", "Forum");
            }

            return View(messageEditView);
        }

        // POST: MessageController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(MessageEditViewModel messageView)
        {
            try
            {
                if (!ModelState.IsValid) 
                {
                    return View(messageView);
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new NullReferenceException($"User with the name {User.Identity.Name} was not found");
                }

                int? topicID = null;

                if (TempData["ForumID"] != null)
                {
                    topicID = (int)TempData["TopicID"];
                }

                await _service.EditAsync(messageView, User);

                TempData["SuccessMessage"] = "Сообщение успешно изменено.";

                if (topicID != null)
                {
                    return RedirectToAction("Details", "Topic", new { id = topicID });
                }
                else
                {
                    return RedirectToAction("Index", "Forum");
                }
            }
            catch (NullReferenceException)
            {
                ViewBag.ErrorMessage = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return View(messageView);
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "У вас недостаточно прав для изменения сообщения.";
                return View(messageView);
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Неправильный запрос. Сообщение не существует или было удалено.";
                return View(messageView);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(messageView);
            }
        }

        // GET: MessageController/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            var messageView = await _service.GetForDeleteAsync(id);


            if (messageView is null)
            {
                TempData["ErrorMessage"] = "Неправильный запрос. Сообщение не существует или уже удалено.";
                return RedirectToAction("Index", "Forum");
            }

            return View(messageView);
        }

        // POST: MessageController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(MessageViewModel messageView)
        {
            try
            {
                int? topicID = null;

                if (TempData["ForumID"] != null)
                {
                    topicID = (int)TempData["TopicID"];
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new NullReferenceException($"User with the name {User.Identity.Name} was not found");
                }

                await _service.DeleteAsync(messageView, User);

                TempData["SuccessMessage"] = "Сообщение успешно удалено.";

                if (topicID != null)
                {
                    return RedirectToAction("Details", "Topic", new { id = topicID });
                }
                else
                {
                    return RedirectToAction("Index", "Forum");
                }
            }
            catch (NullReferenceException)
            {
                ViewBag.ErrorMessage = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return View(messageView);
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Неправильный запрос. Сообщение не существует или было удалено.";
                return View(messageView);
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "У вас недостаточно прав для изменения сообщения.";
                return View(messageView);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(messageView);
            }
        }
    }
}
