﻿using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task3.Services;
using Backend_Task2.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Backend_Task3.Controllers
{
    [AllowAnonymous]
    public class AuthenticationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IUserService _service;
        private readonly UserManager<AppUserModel> _userManager;
        private readonly SignInManager<AppUserModel> _signInManager;

        public AuthenticationController(SignInManager<AppUserModel> signInManager, UserManager<AppUserModel> userManager, IUserService service, ApplicationDbContext context)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _service = service;
            _context = context;
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Register(UserRegisterViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                await _service.RegisterUserAsync(model);

                TempData["SuccessMessage"] = "Аккаунт успешно создан.";

                return RedirectToAction("Index", "Forum");
            }
            catch (ArgumentException)
            {
                ViewBag.ErrorMessage = "Пароли не совпадают.";
                return View(model);
            }
            catch (Exception e)
            {
                if (e.Message == "A user with such name already exists.")
                {
                    ViewBag.ErrorMessage = "Пользователь с таким именем уже существует.";
                    return View(model);
                }
                
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте ещё раз.";
                return View(model);
            }

        }

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> LogIn(UserLogInViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var signInResult = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);
                if (signInResult.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }

                return View(model);
            }
            catch
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте ещё раз.";
                return View(model);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Logout()
        {
            try
            {
                await _signInManager.SignOutAsync();
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
