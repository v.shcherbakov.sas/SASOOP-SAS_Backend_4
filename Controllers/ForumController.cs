﻿using Backend_Task2.Forum;
using Backend_Task3.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend_Task2.ViewModels;
using Backend_Task2.Forum.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace Backend_Task2.Controllers
{
    [Authorize]
    public class ForumController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IForumSectionService _service;
        private readonly UserManager<AppUserModel> _userManager;

        [TempData]
        public int ForumID { get; set; }

        public ForumController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment, IForumSectionService service, UserManager<AppUserModel> userManager)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
            _service = service;
            _userManager = userManager;
        }

        // GET: ForumController
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            try
            {
                var forumSections = await _service.IndexAsync();

                return View(forumSections);
            }
            catch
            {
                TempData["ErrorMessage"] = "Что-то пошло не так, попробуйте позднее.";
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: ForumController/Details/5
        [AllowAnonymous]
        public async Task<ActionResult> Details(int id)
        {
            var forumSection = await _service.DetailsAsync(id);

            if (forumSection is null)
            {
                TempData["ErrorMessage"] = "Неправильный запрос. Форум не существует или был удалён.";
                return RedirectToAction(nameof(Index));
            }

            if (TempData.Peek("ForumID") == null)
            {
                ForumID = id;
            }
            else
            {
                TempData["ForumID"] = id;
            }

            return View(forumSection);
        }

        // GET: ForumController/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View(new ForumSectionCreateViewModel { });
        }

        // POST: ForumController/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Create(ForumSectionCreateViewModel forumModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(forumModel);
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new UnauthorizedAccessException($"User with the name {User.Identity.Name} was not found");
                }

                if (_context.ForumSections.Any(x => x.Name.ToLower() == forumModel.Name.ToLower()))
                {
                    ViewBag.ErrorMessage = "Форум с таким именем уже существует.";
                    return View(forumModel);
                }

                await _service.CreateAsync(forumModel, User);

                return RedirectToAction(nameof(Index));
            }
            catch (UnauthorizedAccessException)
            {
                TempData["ErrorMessage"] = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(forumModel);
            }
        }

        // GET: ForumController/Edit/5
        [Authorize(Roles = "Admin, Moderator")]
        public async Task<ActionResult> Edit(int id)
        {
            var forum = await _service.GetForEditAsync(id);

            if (forum is null)
            {
                TempData["ErrorMessage"] = "Неправильный запрос. Форум не существует или был удалён.";
                return RedirectToAction(nameof(Index));
            }

            return View(forum);
        }

        // POST: ForumController/Edit/5
        [HttpPost]
        [Authorize(Roles = "Admin, Moderator")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ForumSectionEditViewModel editForum)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(editForum);
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new NullReferenceException($"User with the name {User.Identity.Name} was not found");
                }

                await _service.EditAsync(editForum, User);

                TempData["ErrorMessage"] = "Форум успешно изменён";
                return RedirectToAction(nameof(Index));
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Неправильный запрос. Форум не существует или уже удалён.";
                return View(editForum);
            }
            catch (NullReferenceException)
            {
                TempData["ErrorMessage"] = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return RedirectToAction(nameof(Index));
            }
            catch (UnauthorizedAccessException)
            {
                TempData["ErrorMessage"] = "У вас недостаточно прав для выполнения действия.";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(editForum);
            }
        }

        // GET: ForumController/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task <ActionResult> Delete(int id)
        {
            var forumView = await _service.GetForDeleteAsync(id);

            if (forumView is null)
            {
                TempData["ErrorMessage"] = "Неправильный запрос. Форум не существует или уже удалён.";
                return RedirectToAction(nameof(Index));
            }

            return View(forumView);
        }

        // POST: ForumController/Delete/5
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(ForumSectionViewModel model)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new UnauthorizedAccessException($"User with the name {User.Identity.Name} was not found");
                }

                await _service.DeleteAsync(model, User);

                TempData["ErrorMessage"] = "Форум успешно удалён";
                return RedirectToAction(nameof(Index));
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Неправильный запрос. Форум не существует или уже удалён.";
                return View(model);
            }
            catch (UnauthorizedAccessException)
            {
                TempData["ErrorMessage"] = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(model);
            }
        }
    }
}
