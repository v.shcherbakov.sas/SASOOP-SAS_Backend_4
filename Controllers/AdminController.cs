﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend_Task2.ViewModels;
using Backend_Task3.Services;

namespace Backend_Task3.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAdminService _service;

        public AdminController(IAdminService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> AssignUserToModerator()
        {
            try
            {
                var users = await _service.GetAllNonPrivilegedUsersAsync();

                var model = new AssignUserToModeratorViewModel
                {
                    Users = users
                };

                return View(model);
            }
            catch (KeyNotFoundException)
            {
                TempData["ErrorMessage"] = "Отсутствуют пользователи с ролью *ForumUser*.";
                return RedirectToAction("Index" , "Admin");
            }
            catch
            {
                TempData["ErrorMessage"] = "Что-то пошло не так. Действие не было выполнено.";
                return RedirectToAction("Index", "Admin");
            }
        }

        [HttpPost]
        public async Task<ActionResult> AssignUserToModerator(AssignUserToModeratorViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                await _service.AssignRoleToUserAsync(model);

                TempData["SuccessMessage"] = "Пользователь успешно назначен на роль модератора.";

                return RedirectToAction("Index", "Admin");
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Пользователь не существует или был удалён.";
                return View(model);
            }
            catch
            {
                TempData["ErrorMessage"] = "Что-то пошло не так. Действие не было выполнено.";
                return RedirectToAction("Index", "Admin");
            }
        }

        public async Task<ActionResult> AssignModeratorToForum()
        {
            try
            {
                return View(await _service.GetAllUsersForumsAsync());
            }
            catch (KeyNotFoundException)
            {
                TempData["ErrorMessage"] = "Отсутствуют пользователи с ролью *Moderator* или отсутствуют форумы.";
                return RedirectToAction("Index", "Admin");
            }
            catch
            {
                TempData["ErrorMessage"] = "Что-то пошло не так. Действие не было выполнено.";
                return RedirectToAction("Index", "Admin");
            }
        }

        [HttpPost]
        public async Task<ActionResult> AssignModeratorToForum(AssignModeratorToForumViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                await _service.AssignForumToModeratorAsync(model);

                TempData["SuccessMessage"] = "Модератор успешно назначен на форум.";

                return RedirectToAction("Index", "Admin");
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Пользователь не существует или был удалён.";
                return View(model);
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "У выбранного пользователя нет роли *Moderator*.";
                return View(model);
            }
            catch
            {
                TempData["ErrorMessage"] = "Что-то пошло не так. Действие не было выполнено.";
                return RedirectToAction("Index", "Admin");
            }
        }

        public ActionResult RemoveModeratorFromForum()
        {
            return View();
        }

        public ActionResult RemoveUserFromModerator()
        {
            return View();
        }
    }
}
