﻿using AutoMapper;
using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using Backend_Task3.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.Controllers
{
    [Authorize]
    public class TopicController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IMapper _mapper;
        private readonly ITopicService _service;
        private readonly UserManager<AppUserModel> _userManager;


        [TempData]
        public int TopicID { get; set; }

        public TopicController(ApplicationDbContext context, IWebHostEnvironment hostEnvironment, IMapper mapper, ITopicService service, UserManager<AppUserModel> userManager)
        {
            _context = context;
            _hostEnvironment = hostEnvironment;
            _mapper = mapper;
            _service = service;
            _userManager = userManager;
        }

        // GET: TopicController/Details/5
        [AllowAnonymous]
        public async Task<ActionResult> Details(int id)
        {
            try
            {
                if (TempData.Peek("TopicID") == null)
                {
                    TopicID = id;
                }
                else
                {
                    TempData["TopicID"] = id;
                }

                var topicView = await _service.DetailsAsync(id);

                return View(topicView);
            }
            catch (KeyNotFoundException)
            {
                TempData["ErrorMessage"] = "Неправильный запрос. Тема не существует или была удалена.";
                return RedirectToAction("Index", "Forum");
            }
            catch (Exception)
            {
                TempData["ErrorMessage"] = "Что-то пошло не так, попробуйте позднее.";
                return RedirectToAction("Index", "Forum");
            }
        }

        // GET: TopicController/Create
        public ActionResult Create(int id)
        {
            return View(new TopicCreateViewModel { ForumId = id });
        }

        // POST: TopicController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TopicCreateViewModel topicModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(topicModel);
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new UnauthorizedAccessException($"User with the name {User.Identity.Name} was not found");
                }

                topicModel.Created = DateTime.Now;
                await _service.CreateAsync(topicModel, user);

                TempData["SuccessMessage"] = "Тема успешно создана";

                return RedirectToAction("Details", "Forum", new { id = topicModel.ForumId });
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return View(topicModel);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(topicModel);
            }
        }

        // GET: TopicController/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                var topic = await _service.GetForEditAsync(id);

                if (topic is null)
                {
                    ViewBag.ErrorMessage = "Неправильный запрос. Тема не существует или уже удалена.";
                    return RedirectToAction("Index", "Forum");
                }

                return View(topic);
            }
            catch
            {
                TempData["ErrorMessage"] = "Что-то пошло не так, попробуйте позднее.";
                return RedirectToAction("Index", "Forum");
            }
        }

        // POST: TopicController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TopicEditViewModel editedTopic)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(editedTopic);
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new NullReferenceException($"User with the name {User.Identity.Name} was not found");
                }

                int forumID = -1;

                if (TempData["ForumID"] != null)
                {
                    forumID = (int)TempData["ForumID"];
                }

                await _service.EditAsync(editedTopic, User);

                TempData["SuccessMessage"] = "Тема успешно изменена";

                if (forumID > -1)
                {
                    return RedirectToAction("Details", "Forum", new { id = TempData["ForumID"] });
                }
                else
                {
                    return RedirectToAction("Index", "Forum");
                }
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Неправильный запрос. Тема не существует или удалена.";
                return View(editedTopic);
            }
            catch (NullReferenceException)
            {
                ViewBag.ErrorMessage = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return View(editedTopic);
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "У вас недостаточно прав для выполнения действия.";
                return View(editedTopic);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(editedTopic);
            }
        }

        // GET: TopicController/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var topic = await _service.GetForDeleteAsync(id);

                if (topic is null)
                {
                    ViewBag.ErrorMessage = "Неправильный запрос. Тема не существует или уже удалена.";
                    return RedirectToAction("Index", "Forum");
                }

                return View(topic);
            }
            catch
            {
                TempData["ErrorMessage"] = "Что-то пошло не так, попробуйте позднее.";
                return RedirectToAction("Index", "Forum");
            }
        }

        // POST: TopicController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(TopicViewModel model)
        {
            try
            {
                int forumID = -1;

                if (TempData["ForumID"] != null)
                {
                    forumID = (int)TempData["ForumID"];
                }

                var user = await _userManager.FindByNameAsync(User.Identity.Name);

                if (user is null)
                {
                    throw new NullReferenceException($"User with the name {User.Identity.Name} was not found");
                }

                await _service.DeleteAsync(model, User);

                TempData["SuccessMessage"] = "Тема успешно удалена";

                if (forumID > -1)
                {
                    return RedirectToAction("Details", "Forum", new { id = TempData["ForumID"] });
                }
                else
                {
                    return RedirectToAction("Index", "Forum");
                }
            }
            catch (KeyNotFoundException)
            {
                ViewBag.ErrorMessage = "Неправильный запрос. Тема не существует или удалена.";
                return View(model);
            }
            catch (NullReferenceException)
            {
                ViewBag.ErrorMessage = "Вы не вошли в аккаунт или он был удалён. Действие отменено.";
                return View(model);
            }
            catch (UnauthorizedAccessException)
            {
                ViewBag.ErrorMessage = "У вас недостаточно прав для выполнения действия.";
                return View(model);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "Что-то пошло не так, попробуйте позднее.";
                return View(model);
            }
        }
    }
}
