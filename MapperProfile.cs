﻿using AutoMapper;
using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<ForumSectionModel, ForumSectionViewModel>();
            CreateMap<ForumSectionModel, ForumSectionEditViewModel>();
            CreateMap<ForumSectionCreateViewModel, ForumSectionModel>();

            CreateMap<TopicModel, TopicViewModel>()
                .ForMember(dest => dest.ForumSectionID, src => src.MapFrom(src => src.ForumSection.Id));
            CreateMap<TopicModel, TopicEditViewModel>();
            CreateMap<TopicCreateViewModel, TopicModel>()
                .ForMember(dest => dest.ForumSection, src => src.Ignore());

            CreateMap<MessageModel, MessageViewModel>()
                .ForMember(dest => dest.TopicID, src => src.MapFrom(src => src.Topic.Id));
            CreateMap<MessageModel, MessageViewModel>()
                .ForMember(dest => dest.TopicID, src => src.MapFrom(src => src.Topic.Id));
            CreateMap<MessageCreateViewModel, MessageModel>()
                .ForMember(dest => dest.Attachments, src => src.Ignore());

            CreateMap<AppUserModel, AppUserViewModel>();

            CreateMap<AttachmentModel, AttachmentViewModel>();
        }
    }
}
