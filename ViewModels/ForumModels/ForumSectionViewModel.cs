﻿using Backend_Task2.Forum.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.ViewModels
{
    public class ForumSectionViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Название форума")]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<TopicViewModel> Topics { get; set; }
    }
}
