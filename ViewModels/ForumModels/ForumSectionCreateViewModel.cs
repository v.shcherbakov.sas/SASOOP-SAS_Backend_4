﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.ViewModels
{
    public class ForumSectionCreateViewModel
    {
        [Required(ErrorMessage = "Это поле необходимо для заполнения.")]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
