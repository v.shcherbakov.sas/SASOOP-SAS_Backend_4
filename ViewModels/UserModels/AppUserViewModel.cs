﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.ViewModels
{
    public class AppUserViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}
