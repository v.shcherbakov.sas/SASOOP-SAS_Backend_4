﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Backend_Task2.ViewModels
{
    public class AssignModeratorToForumViewModel
    {
        [Required]
        public string UserId { get; set; }

        public List<AppUserViewModel> Users { get; set; }

        [Required]
        public int? ForumId { get; set; }

        public List<ForumSectionViewModel> Forums {get; set;}
    }
}
