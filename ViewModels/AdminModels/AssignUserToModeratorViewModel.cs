﻿using Backend_Task2.Forum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Backend_Task2.ViewModels
{
    public class AssignUserToModeratorViewModel
    {
        [Required]
        public string UserId { get; set; }

        public List<AppUserViewModel> Users { get; set; }
    }
}
