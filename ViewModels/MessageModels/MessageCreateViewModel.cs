﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.ViewModels
{
    public class MessageCreateViewModel
    {
        [Required(ErrorMessage = "Это поле необходимо для заполнения.")]
        public string Text { get; set; }

        public int TopicID { get; set; }

        public DateTime Created { get; set; }

        public IFormFileCollection Attachments { get; set; }
    }
}
