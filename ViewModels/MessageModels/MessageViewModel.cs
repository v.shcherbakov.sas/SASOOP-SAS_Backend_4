﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.ViewModels
{
    public class MessageViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Modified { get; set; }

        public int TopicID { get; set; }

        public List<AttachmentViewModel> Attachments { get; set; }
    }
}
