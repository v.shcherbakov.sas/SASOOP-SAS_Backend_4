﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.ViewModels
{
    public class MessageEditViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Текст сообщения не может быть пустым.")]
        [MaxLength(500)]
        public string Text { get; set; }

        public DateTime Modified { get; set; }
    }
}
