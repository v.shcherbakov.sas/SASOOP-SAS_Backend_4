using AutoMapper;
using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task3.Forum.Roles;
using Backend_Task3.Services;
using Backend_Task4.APIServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString));

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MapperProfile());
            });

            var mapper = mapperConfig.CreateMapper();

            services.AddSingleton(mapper);

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;
            });

            services.AddIdentity<AppUserModel, IdentityRole>(options =>
                options.SignIn.RequireConfirmedAccount = false)
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<IUserService, UserService>();

            services.AddScoped<IForumSectionService, ForumSectionService>();
            services.AddScoped<ITopicService, TopicService>();
            services.AddScoped<IMessageService, MessageService>();
            services.AddScoped<IAdminService, AdminService>();

            services.AddScoped<IForumAPIService, ForumAPIService>();
            services.AddScoped<ITopicAPIService, TopicAPIService>();
            services.AddScoped<IMessageAPIService, MessageAPIService>();

            services.AddControllersWithViews();
            InitRoles(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ApplicationDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            context.Database.Migrate();

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private async void InitRoles(IServiceCollection services)
        {
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();
                var context = serviceProvider.GetService<ApplicationDbContext>();
                var userManager = serviceProvider.GetService<UserManager<AppUserModel>>();

                foreach (var role in ForumRoles.AllRoles)
                {
                    if (roleManager.Roles.Any(x => x.Name == role))
                    {
                        continue;
                    }

                    var result = await roleManager.CreateAsync(
                        new IdentityRole
                        {
                            Id = Guid.NewGuid().ToString(),
                            Name = role
                        });
                }

                var admin = new AppUserModel
                {
                    UserName = "Admin",
                    NormalizedUserName = "ADMIN"
                };
                var moderator1 = new AppUserModel
                {
                    UserName = "Moderator1",
                    NormalizedUserName = "MODERATOR1"
                };
                var moderator2 = new AppUserModel
                {
                    UserName = "Moderator2",
                    NormalizedUserName = "MODERATOR2"
                };
                var moderator3 = new AppUserModel
                {
                    UserName = "Moderator3",
                    NormalizedUserName = "MODERATOR3"
                };

                if (!context.Users.Any(x => x.UserName == "Admin"))
                {
                    await userManager.CreateAsync(admin, "123456");
                    await userManager.AddToRoleAsync(admin, "Admin");
                }
                if (!context.Users.Any(x => x.UserName == "Moderator1"))
                {
                    await userManager.CreateAsync(moderator1, "qwerty");
                    await userManager.AddToRoleAsync(moderator1, "Moderator");
                }
                if (!context.Users.Any(x => x.UserName == "Moderator2"))
                {
                    await userManager.CreateAsync(moderator2, "aezakmi");
                    await userManager.AddToRoleAsync(moderator2, "Moderator");
                }
                if (!context.Users.Any(x => x.UserName == "Moderator3"))
                {
                    await userManager.CreateAsync(moderator3, "aspnet");
                    await userManager.AddToRoleAsync(moderator3, "Moderator");
                }
            }
        }
    }
}
