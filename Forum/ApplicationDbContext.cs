﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend_Task2.Forum.Models;
using Microsoft.AspNetCore.Identity;


namespace Backend_Task2.Forum
{
    public class ApplicationDbContext : IdentityDbContext<AppUserModel>
    {
        public DbSet<ForumSectionModel> ForumSections { get; set; }
        public DbSet<TopicModel> Topics { get; set; }
        public DbSet<MessageModel> Messages { get; set; }
        public DbSet<AttachmentModel> Attachments { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ForumSectionModel>().HasMany(x => x.Topics).WithOne(x => x.ForumSection);

            modelBuilder.Entity<ForumToModerator>().HasOne(x => x.ForumSection).WithMany(x => x.ForumToModerator);
            modelBuilder.Entity<ForumToModerator>().HasOne(x => x.SectionModerator).WithMany(x => x.ModeratedForums);

            modelBuilder.Entity<TopicModel>().HasMany(x => x.Messages).WithOne(x => x.Topic);
            modelBuilder.Entity<TopicModel>().HasOne(x => x.Author).WithMany(x => x.Topics);

            modelBuilder.Entity<MessageModel>().HasMany(x => x.Attachments).WithOne(x => x.Message);
            modelBuilder.Entity<MessageModel>().HasOne(x => x.Author).WithMany(x => x.Messages);
        }
    }
}



/*var admin = new IdentityUser
{
    Id = Guid.NewGuid().ToString(),
    UserName = "Admin",
    NormalizedUserName = "ADMIN"
};

var moderator1 = new IdentityUser
{
    Id = Guid.NewGuid().ToString(),
    UserName = "Moderator1",
    NormalizedUserName = "MODERATOR1"
};

var moderator2 = new IdentityUser
{
    Id = Guid.NewGuid().ToString(),
    UserName = "Moderator2",
    NormalizedUserName = "MODERATOR2"
};

var moderator3 = new IdentityUser
{
    Id = Guid.NewGuid().ToString(),
    UserName = "Moderator3",
    NormalizedUserName = "MODERATOR3"
};



modelBuilder.Entity<IdentityUser>().HasData(admin);
modelBuilder.Entity<IdentityUser>().HasData(moderator1);
modelBuilder.Entity<IdentityUser>().HasData(moderator2);
modelBuilder.Entity<IdentityUser>().HasData(moderator3);

modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
{
    RoleId = "Admin",
    UserId = admin.Id
});
modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
{
    RoleId = "Moderator",
    UserId = moderator1.Id
});
modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
{
    RoleId = "Moderator",
    UserId = moderator2.Id
});
modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
{
    RoleId = "Moderator",
    UserId = moderator3.Id
});*/