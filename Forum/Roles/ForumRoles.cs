﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task3.Forum.Roles
{
    public static class ForumRoles
    {
        public static readonly string Admin = "Admin";
        public static readonly string Moderator = "Moderator";
        public static readonly string ForumUser = "ForumUser";

        public static readonly string[] AllRoles = new string[] { Admin, Moderator, ForumUser };
    }
}
