﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.Forum.Models
{
    public class ForumToModerator
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public AppUserModel SectionModerator { get; set; }
        [Required]
        public ForumSectionModel ForumSection { get; set; }
    }
}
