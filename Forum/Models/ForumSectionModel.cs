﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.Forum.Models
{
    public class ForumSectionModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Description { get; set; }

        public ICollection<TopicModel> Topics { get; set; }

        public IEnumerable<ForumToModerator> ForumToModerator { get; set; }
    }
}
