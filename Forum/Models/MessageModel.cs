﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.Forum.Models
{
    public class MessageModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(500)]
        public string Text { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Modified { get; set; }

        [Required]
        public TopicModel Topic { get; set; }

        public IEnumerable<AttachmentModel> Attachments { get; set; }

        public AppUserModel Author { get; set; }
    }
}
