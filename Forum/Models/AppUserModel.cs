﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task2.Forum.Models
{
    public class AppUserModel : IdentityUser
    {
        public IEnumerable<ForumToModerator> ModeratedForums { get; set; }
        public IEnumerable<MessageModel> Messages { get; set; }
        public IEnumerable<TopicModel> Topics { get; set; }
    }
}
