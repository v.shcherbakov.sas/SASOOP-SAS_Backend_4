﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task2.Forum.Models
{
    public class TopicModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string Description { get; set; }

        public DateTime Created { get; set; }

        [Required]
        public ForumSectionModel ForumSection { get; set; }

        public IEnumerable<MessageModel> Messages { get; set; }

        public AppUserModel Author { get; set; }
    }
}
