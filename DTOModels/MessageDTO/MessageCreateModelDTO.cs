﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Backend_Task4.DTOModels
{
    public class MessageCreateModelDTO
    {
        [JsonPropertyName("text")]
        public string Text { get; set; }
    }
}
