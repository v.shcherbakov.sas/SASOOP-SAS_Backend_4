﻿using Backend_Task4.APIServices;
using Backend_Task4.DTOModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIControllers
{
    [ApiController]
    [Route("api")]
    public class MessageAPIController : ControllerBase
    {
        private readonly IMessageAPIService _service;

        public MessageAPIController(IMessageAPIService service)
        {
            _service = service;
        }

        [Route("topics/{id}/messages")]
        [HttpGet]
        public async Task<ActionResult> Get(int id)
        {
            try
            {
                var topics = await _service.GetMessagesAsync(id);
                return Ok(topics);
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("topics/{id}/messages")]
        [HttpPost]
        public async Task<ActionResult> Post(MessageCreateModelDTO model, int id)
        {
            try
            {
                if (model is null)
                {
                    return BadRequest();
                }

                if (string.IsNullOrWhiteSpace(model.Text))
                {
                    return BadRequest();
                }

                await _service.CreateMessageAsync(model, id);

                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("messages/{id}")]
        [HttpPut]
        public async Task<ActionResult> Put(MessageEditModelDTO model, int id)
        {
            try
            {
                if (model is null)
                {
                    return BadRequest();
                }

                if (string.IsNullOrWhiteSpace(model.Text))
                {
                    return BadRequest();
                }

                await _service.EditMessageAsync(model, id);

                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("messages/{id}")]
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteMessageAsync(id);

                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
