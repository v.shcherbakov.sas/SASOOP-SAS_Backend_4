﻿using Backend_Task4.APIServices;
using Backend_Task4.DTOModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Backend_Task4.APIControllers
{
    [ApiController]
    [Route("api/sections")]
    public class ForumAPIController : ControllerBase
    {
        private readonly IForumAPIService _service;

        public ForumAPIController(IForumAPIService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult> Get() 
        {
            try
            {
                var forums = await _service.GetForumsAsync();
                return Ok(forums);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(ForumCreateModelDTO model)
        {
            try
            {
                if (model is null)
                {
                    return BadRequest();
                }

                if (string.IsNullOrWhiteSpace(model.Name))
                {
                    return BadRequest();
                }

                await _service.AddForumAsync(model);
                
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<ActionResult> Put(ForumEditModelDTO model, int id)
        {
            try
            {
                if (model is null)
                {
                    return BadRequest();
                }

                if (string.IsNullOrWhiteSpace(model.Name))
                {
                    return BadRequest();
                }

                await _service.EditForumAsync(model, id);

                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
