﻿using Backend_Task4.APIServices;
using Backend_Task4.DTOModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIControllers
{
    [ApiController]
    [Route("api")]
    public class TopicAPIController : ControllerBase
    {
        private readonly ITopicAPIService _service;

        public TopicAPIController(ITopicAPIService service)
        {
            _service = service;
        }

        [Route("sections/{forumId}/topics")]
        [HttpGet]
        public async Task<ActionResult> Get(int forumId)
        {
            try
            {
                var topics = await _service.GetTopicsAsync(forumId);
                return Ok(topics);
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("sections/{id}/topics")]
        [HttpPost]
        public async Task<ActionResult> Post(TopicCreateModelDTO model, int id)
        {
            try
            {
                if (model is null)
                {
                    return BadRequest();
                }

                if (string.IsNullOrWhiteSpace(model.Name))
                {
                    return BadRequest();
                }

                await _service.CreateTopicAsync(model, id);

                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("topics/{id}")]
        [HttpPut]
        public async Task<ActionResult> Put(TopicEditModelDTO model, int id)
        {
            try
            {
                if (model is null)
                {
                    return BadRequest();
                }

                if (string.IsNullOrWhiteSpace(model.Name))
                {
                    return BadRequest();
                }

                await _service.EditTopicAsync(model, id);

                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("topics/{id}")]
        [HttpDelete]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _service.DeleteTopicAsync(id);

                return Ok();
            }
            catch (KeyNotFoundException)
            {
                return StatusCode(404);
            }
            catch
            {
                return StatusCode(500);
            }
        }
    }
}
