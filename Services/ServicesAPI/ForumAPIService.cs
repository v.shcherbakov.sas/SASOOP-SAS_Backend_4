﻿using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task4.DTOModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIServices
{
    public class ForumAPIService : IForumAPIService
    {
        public readonly ApplicationDbContext _context;

        public ForumAPIService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task AddForumAsync(ForumCreateModelDTO model)
        {
            var forum = new ForumSectionModel { 
                Name = model.Name,
                Description = model.Description
            };

            _context.Add(forum);

            await _context.SaveChangesAsync();
        }

        public async Task EditForumAsync(ForumEditModelDTO model, int id)
        {
            var forum = await _context.ForumSections.FirstOrDefaultAsync(x => x.Id == id);

            if (forum is null)
            {
                throw new KeyNotFoundException();
            }

            forum.Name = model.Name;
            forum.Description = model.Description;

            await _context.SaveChangesAsync();
        }

        public async Task<List<ForumModelDTO>> GetForumsAsync()
        {
            var forums = await _context.ForumSections.Select(x => new ForumModelDTO
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description
            }).ToListAsync();

            return forums;
        }
    }
}
