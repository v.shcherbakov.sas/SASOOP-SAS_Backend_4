﻿using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task4.DTOModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIServices
{
    public class MessageAPIService : IMessageAPIService
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<AppUserModel> _manager;

        public MessageAPIService(ApplicationDbContext context, UserManager<AppUserModel> manager)
        {
            _context = context;
            _manager = manager;
        }

        public async Task CreateMessageAsync(MessageCreateModelDTO model, int topicId)
        {
            var topic = await _context.Topics.FirstOrDefaultAsync(x => x.Id == topicId);
            var user = await _manager.FindByNameAsync("Admin");

            if (topic is null)
            {
                throw new KeyNotFoundException("Topic was not found.");
            }

            if (user is null)
            {
                throw new KeyNotFoundException("User was not found.");
            }

            var message = new MessageModel
            {
                Topic = topic,
                Text = model.Text,
                Created = DateTime.Now,
                Author = user
            };

            _context.Add(message);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteMessageAsync(int messageId)
        {
            var message = await _context.Messages.FirstOrDefaultAsync(x => x.Id == messageId);

            if (message is null)
            {
                throw new KeyNotFoundException();
            }

            _context.Remove(message);

            await _context.SaveChangesAsync();
        }

        public async Task EditMessageAsync(MessageEditModelDTO model, int messageId)
        {
            var message = await _context.Messages.FirstOrDefaultAsync(x => x.Id == messageId);

            if (message is null)
            {
                throw new KeyNotFoundException();
            }

            message.Text = model.Text;
            message.Modified = DateTime.Now;

            await _context.SaveChangesAsync();
        }

        public async Task<List<MessageModelDTO>> GetMessagesAsync(int topicId)
        {
            var topic = await _context.Topics.Include(x => x.Messages).FirstOrDefaultAsync(x => x.Id == topicId);

            if (topic is null)
            {
                throw new KeyNotFoundException();
            }

            var messages = topic.Messages.Select(x => new MessageModelDTO
            {
                Id = x.Id,
                Text = x.Text,
                Created = x.Created,
                Modified = x.Modified
            }).ToList();

            return messages;
        }
    }
}
