﻿using Backend_Task4.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIServices
{
    public interface ITopicAPIService
    {
        Task<List<TopicModelDTO>> GetTopicsAsync(int forumId);
        Task CreateTopicAsync(TopicCreateModelDTO model, int forumId);
        Task EditTopicAsync(TopicEditModelDTO model, int topicId);
        Task DeleteTopicAsync(int topicId);
    }
}
