﻿using Backend_Task4.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIServices
{
    public interface IForumAPIService
    {
        Task<List<ForumModelDTO>> GetForumsAsync();
        Task AddForumAsync(ForumCreateModelDTO model);
        Task EditForumAsync(ForumEditModelDTO model, int id);
    }
}
