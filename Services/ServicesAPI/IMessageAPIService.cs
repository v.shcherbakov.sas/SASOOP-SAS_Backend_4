﻿using Backend_Task4.DTOModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIServices
{
    public interface IMessageAPIService
    {
        Task<List<MessageModelDTO>> GetMessagesAsync(int topicId);
        Task CreateMessageAsync(MessageCreateModelDTO model, int topicId);
        Task EditMessageAsync(MessageEditModelDTO model, int messageId);
        Task DeleteMessageAsync(int messageId);
    }
}
