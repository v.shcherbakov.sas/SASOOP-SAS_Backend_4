﻿using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task4.DTOModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task4.APIServices
{
    public class TopicAPIService : ITopicAPIService
    {
        private readonly ApplicationDbContext _context;

        public TopicAPIService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task CreateTopicAsync(TopicCreateModelDTO model, int forumId)
        {
            var forum = await _context.ForumSections.FirstOrDefaultAsync(x => x.Id == forumId);

            if (forum is null)
            {
                throw new KeyNotFoundException();
            }

            var topic = new TopicModel { 
                Name = model.Name,
                Description = model.Description,
                ForumSection = forum
            };

            _context.Add(topic);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteTopicAsync(int topicId)
        {
            var topic = await _context.Topics.FirstOrDefaultAsync(x => x.Id == topicId);

            if (topic is null)
            {
                throw new KeyNotFoundException();
            }

            _context.Remove(topic);

            await _context.SaveChangesAsync();
        }

        public async Task EditTopicAsync(TopicEditModelDTO model, int topicId)
        {
            var topic = await _context.Topics.FirstOrDefaultAsync(x => x.Id == topicId);

            if (topic is null)
            {
                throw new KeyNotFoundException();
            }

            topic.Name = model.Name;
            topic.Description = model.Description;

            await _context.SaveChangesAsync();
        }

        public async Task<List<TopicModelDTO>> GetTopicsAsync(int forumId)
        {
            var forum = await _context.ForumSections.Include(x => x.Topics).FirstOrDefaultAsync(x => x.Id == forumId);

            if (forum is null)
            {
                throw new KeyNotFoundException();
            }

            var topics = forum.Topics.Select(x => new TopicModelDTO {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                Created = x.Created,
            }).ToList();

            return topics;
        }
    }
}
