﻿using Backend_Task2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public interface IUserService
    {
        public Task RegisterUserAsync(UserRegisterViewModel model);

        public Task LogoutAsync();

        public Task LogInAsync();
    }
}
