﻿using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public interface IForumSectionService
    {
        Task CreateAsync(ForumSectionCreateViewModel model, ClaimsPrincipal user);
        Task<ForumSectionViewModel> DetailsAsync(int id);
        Task<List<ForumSectionViewModel>> IndexAsync();
        Task<ForumSectionEditViewModel> GetForEditAsync(int id);
        Task EditAsync(ForumSectionEditViewModel model, ClaimsPrincipal user);
        Task<ForumSectionViewModel> GetForDeleteAsync(int id);
        Task DeleteAsync(ForumSectionViewModel model, ClaimsPrincipal user);
    }
}
