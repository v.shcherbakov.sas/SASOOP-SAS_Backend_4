﻿using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public interface IAdminService
    {
        public Task<AssignModeratorToForumViewModel> GetAllUsersForumsAsync();

        public Task<IList<AppUserModel>> GetAllModeratorsAsync();

        public Task AssignRoleToUserAsync(AssignUserToModeratorViewModel model);

        public Task AssignForumToModeratorAsync(AssignModeratorToForumViewModel model);

        public Task RemoveRoleFromUserAsync(ClaimsPrincipal model);

        public Task RemoveForumFromModeratorAsync(ClaimsPrincipal model, ForumSectionViewModel forumViewModel);

        public Task<List<AppUserViewModel>> GetAllNonPrivilegedUsersAsync();
    }
}
