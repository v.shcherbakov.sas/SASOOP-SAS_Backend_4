﻿using AutoMapper;
using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Security.Claims;

namespace Backend_Task3.Services
{
    public class TopicService : ITopicService
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IMapper _mapper;

        public TopicService(ApplicationDbContext context, IMapper mapper, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _mapper = mapper;
            _hostEnvironment = hostEnvironment;
        }

        public async Task CreateAsync(TopicCreateViewModel model, AppUserModel user)
        {
            var topic = _mapper.Map<TopicModel>(model);
            topic.ForumSection = await _context.ForumSections.FirstOrDefaultAsync(x => x.Id == model.ForumId);
            topic.Author = user;
            
            if (topic.ForumSection is null)
            {
                throw new KeyNotFoundException();
            }

            _context.Topics.Add(topic);
            await _context.SaveChangesAsync();
            
        }

        public async Task DeleteAsync(TopicViewModel model, ClaimsPrincipal user)
        {
            TopicModel topic = await _context.Topics
                    .Include(x => x.ForumSection).ThenInclude(x => x.ForumToModerator).ThenInclude(x => x.SectionModerator)
                    .Include(x => x.Messages).ThenInclude(x => x.Attachments)
                    .Include(x => x.Author)
                    .FirstOrDefaultAsync(x => x.Id == model.Id);

            if (topic is null)
            {
                throw new KeyNotFoundException();
            }

            if (!user.IsInRole("Admin") &&
                !(user.IsInRole("Moderator") && topic.ForumSection.ForumToModerator.Any(x => x.SectionModerator.UserName == user.Identity.Name)) &&
                !(topic.Author.UserName == user.Identity.Name))
            {
                throw new UnauthorizedAccessException("Not enough privileges to perform the action");
            }

            foreach (var message in topic.Messages)
            {
                if (!(message.Attachments is null))
                {
                    foreach (var image in message.Attachments)
                    {
                        System.IO.File.Delete(_hostEnvironment.WebRootPath + image.FilePath);
                    }
                }
            }

            _context.Remove(topic);
            await _context.SaveChangesAsync();
        }

        public async Task<TopicViewModel> DetailsAsync(int id)
        {
            var topic = await _context.Topics
                .Include(x => x.Messages).ThenInclude(x => x.Attachments)
                .FirstOrDefaultAsync(x => x.Id == id);

            topic.Messages = topic.Messages.OrderByDescending(x => x.Created);

            if (topic is null)
            {
                throw new KeyNotFoundException();
            }

            return _mapper.Map<TopicViewModel>(topic);
        }

        public async Task EditAsync(TopicEditViewModel model, ClaimsPrincipal user)
        {
            TopicModel topic = await _context.Topics
                    .Include(x => x.ForumSection).ThenInclude(x => x.ForumToModerator).ThenInclude(x => x.SectionModerator)
                    .Include(x => x.Messages).ThenInclude(x => x.Attachments)
                    .Include(x => x.Author)
                    .FirstOrDefaultAsync(x => x.Id == model.Id);

            if (topic is null)
            {
                throw new KeyNotFoundException();
            }

            if (!user.IsInRole("Admin") &&
                !(user.IsInRole("Moderator") && topic.ForumSection.ForumToModerator.Any(x => x.SectionModerator.UserName == user.Identity.Name)) &&
                !(topic.Author.UserName == user.Identity.Name))
            {
                throw new UnauthorizedAccessException("Not enough privileges to perform the action");
            }

            topic.Name = model.Name;
            topic.Description = model.Description;

            await _context.SaveChangesAsync();
        }

        public async Task<TopicViewModel> GetForDeleteAsync(int id)
        {
            var topic = await _context.Topics.FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<TopicViewModel>(topic);
        }

        public async Task<TopicEditViewModel> GetForEditAsync(int id)
        {
            var topic = await _context.Topics.FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<TopicEditViewModel>(topic);
        }
    }
}
