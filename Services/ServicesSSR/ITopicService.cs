﻿using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public interface ITopicService
    {
        Task CreateAsync(TopicCreateViewModel model, AppUserModel user);
        Task<TopicViewModel> DetailsAsync(int id);
        Task<TopicEditViewModel> GetForEditAsync(int id);
        Task EditAsync(TopicEditViewModel model, ClaimsPrincipal user);
        Task<TopicViewModel> GetForDeleteAsync(int id);
        Task DeleteAsync(TopicViewModel model, ClaimsPrincipal user);
    }
}
