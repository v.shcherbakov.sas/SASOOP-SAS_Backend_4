﻿using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public interface IMessageService
    {
        Task CreateAsync(MessageCreateViewModel model, AppUserModel user);
        Task<MessageEditViewModel> GetForEditAsync(int id);
        Task EditAsync(MessageEditViewModel model, ClaimsPrincipal user);
        Task<MessageViewModel> GetForDeleteAsync(int id);
        Task DeleteAsync(MessageViewModel model, ClaimsPrincipal user);
    }
}
