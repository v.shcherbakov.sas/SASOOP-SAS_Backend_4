﻿using AutoMapper;
using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public class MessageService : IMessageService
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IMapper _mapper;

        public MessageService(ApplicationDbContext context, IMapper mapper, IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _mapper = mapper;
            _hostEnvironment = hostEnvironment;
        }

        public async Task CreateAsync(MessageCreateViewModel newMessage, AppUserModel user)
        {
            try
            {
                var message = _mapper.Map<MessageModel>(newMessage);

                message.Topic = await _context.Topics.FirstOrDefaultAsync(x => x.Id == newMessage.TopicID);

                if (message.Topic is null)
                {
                    throw new KeyNotFoundException("Тема для отправки сообщения не найдена.");
                }

                if (newMessage.Attachments != null)
                {
                    try
                    {
                        CheckUploadedFiles(newMessage);
                    }
                    catch (Exception e)
                    {
                        throw new Exception($"{e.Message}");
                    }

                    foreach (IFormFile attachment in newMessage.Attachments)
                    {
                        var attachmentModel = new AttachmentModel { };

                        var created = message.Created;

                        attachmentModel.FileName = Path.GetFileNameWithoutExtension(attachment.FileName);

                        var fileName = DateTime.Now.ToString("yyyyMMddhhmmssff");
                        var fileExtension = Path.GetExtension(attachment.FileName);

                        attachmentModel.FilePath = $"\\images\\{fileName + fileExtension}";

                        var filePath = Path.Combine(_hostEnvironment.WebRootPath + attachmentModel.FilePath);

                        attachmentModel.Created = created;
                        attachmentModel.Message = message;

                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            attachment.CopyTo(fileStream);
                        }

                        _context.Add(attachmentModel);
                    }
                }

                message.Author = user;
                _context.Add(message);
                await _context.SaveChangesAsync();
            }
            catch
            {
                
            }

        }

        public async Task DeleteAsync(MessageViewModel model, ClaimsPrincipal user)
        {
            var message = await _context.Messages
                .Include(x => x.Topic).ThenInclude(x => x.ForumSection).ThenInclude(x => x.ForumToModerator).ThenInclude(x => x.SectionModerator)
                .Include(x => x.Author)
                .Include(x => x.Attachments).FirstOrDefaultAsync(x => x.Id == model.Id);

            if (message is null)
            {
                throw new KeyNotFoundException();
            }

            if (!user.IsInRole("Admin") &&
                !(user.IsInRole("Moderator") && message.Topic.ForumSection.ForumToModerator.Any(x => x.SectionModerator.UserName == user.Identity.Name)) &&
                !(message.Author.UserName == user.Identity.Name))
            {
                throw new UnauthorizedAccessException("Not enough privileges to perform the action");
            }

            if (!(message.Attachments is null))
            {
                foreach (var image in message.Attachments)
                {
                    System.IO.File.Delete(_hostEnvironment.WebRootPath + image.FilePath);
                }
            }

            _context.Remove(message);
            await _context.SaveChangesAsync();
        }

        public async Task EditAsync(MessageEditViewModel model, ClaimsPrincipal user)
        {
            var message = await _context.Messages
                .Include(x => x.Topic).ThenInclude(x => x.ForumSection).ThenInclude(x => x.ForumToModerator).ThenInclude(x => x.SectionModerator)
                .Include(x => x.Author)
                .FirstOrDefaultAsync(x => x.Id == model.Id);

            if (message is null)
            {
                throw new KeyNotFoundException();
            }

            if (!user.IsInRole("Admin") &&
                !(user.IsInRole("Moderator") && message.Topic.ForumSection.ForumToModerator.Any(x => x.SectionModerator.UserName == user.Identity.Name)) &&
                !(message.Author.UserName == user.Identity.Name))
            {
                throw new UnauthorizedAccessException("Not enough privileges to perform the action");
            }

            message.Text = model.Text;
            message.Modified = model.Modified;

            await _context.SaveChangesAsync();
        }

        public async Task<MessageViewModel> GetForDeleteAsync(int id)
        {
            var message = await _context.Messages.Include(x => x.Topic).FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<MessageViewModel>(message);
        }

        public async Task<MessageEditViewModel> GetForEditAsync(int id)
        {
            var message = await _context.Messages.Include(x => x.Topic).FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<MessageEditViewModel>(message);
        }

        private void CheckUploadedFiles(MessageCreateViewModel newMessage )
        {
            if (newMessage.Attachments.Count > 4)
            {
                newMessage.Attachments = null;
                throw new Exception("Превышено количество загружаемых файлов.");
            }

            var allowedExtensions = new List<string> { ".jpeg", ".jpg", ".png" };

            foreach (IFormFile attachment in newMessage.Attachments)
            {
                if (attachment.Length > 8388608)
                {
                    throw new Exception("Максимальный размер изображения - 8МБ");
                }

                var fileExtension = Path.GetExtension(attachment.FileName);

                if (!allowedExtensions.Any(x => x == fileExtension))
                {
                    throw new Exception("Загружать можно только изображения формата png/jpg/jpeg");
                }
            }
        }
    }
}
