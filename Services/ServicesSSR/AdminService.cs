﻿using AutoMapper;
using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public class AdminService : IAdminService
    {
        private readonly UserManager<AppUserModel> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public AdminService(UserManager<AppUserModel> userManager, ApplicationDbContext context, IMapper mapper)
        {
            _userManager = userManager;
            _context = context;
            _mapper = mapper;
        }

        public async Task AssignForumToModeratorAsync(AssignModeratorToForumViewModel model)
        {
            var userModel = await _userManager.FindByIdAsync(model.UserId);

            if (userModel is null)
            {
                throw new KeyNotFoundException($"User was not found.");
            }

            var userRoles = await _userManager.GetRolesAsync(userModel);

            if (!userRoles.Contains("Moderator"))
            {
                throw new UnauthorizedAccessException("Chosen user does not have enough privileges to perform the action");
            }

            var forum = await _context.ForumSections.FirstOrDefaultAsync(x => x.Id == model.ForumId);

            if (forum is null)
            {
                throw new KeyNotFoundException($"Forum was not found.");
            }

            var moderatedForum = new ForumToModerator
            {
                SectionModerator = userModel,
                ForumSection = forum
            };

            _context.Add(moderatedForum);
            await _context.SaveChangesAsync();
        }

        public async Task AssignRoleToUserAsync(AssignUserToModeratorViewModel model)
        {
            var user = await _userManager.FindByIdAsync(model.UserId);

            if (user is null)
            {
                throw new KeyNotFoundException($"User was not found.");
            }

            
            var result = await _userManager.AddToRoleAsync(user, "Moderator");

            if (!result.Succeeded)
            {
                throw new TaskCanceledException("Something went wrong. Please, try again");
            }

            result = await _userManager.RemoveFromRoleAsync(user, "ForumUser");
        }

        public async Task<IList<AppUserModel>> GetAllModeratorsAsync()
        {
            var users = await _userManager.GetUsersInRoleAsync("Moderator");

            return users;
        }

        public async Task<List<AppUserViewModel>> GetAllNonPrivilegedUsersAsync()
        {
            var users = await _userManager.GetUsersInRoleAsync("ForumUser");
            
            if (users.Count == 0)
            {
                throw new KeyNotFoundException("There is no users to choose from");
            }

            return users.Select(x => _mapper.Map<AppUserViewModel>(x)).ToList();
        }

        public async Task<AssignModeratorToForumViewModel> GetAllUsersForumsAsync()
        {
            var users = await _userManager.GetUsersInRoleAsync("Moderator");
            var forums = await _context.ForumSections
                .Select(x => _mapper.Map<ForumSectionViewModel>(x)).ToListAsync();

            if (users is null || forums is null)
            {
                throw new Exception("One or two fields for choosing are empty");
            }

            var userViews = users.Select(x => _mapper.Map<AppUserViewModel>(x)).ToList();

            var moderatorSelect = new AssignModeratorToForumViewModel
            {
                Users = userViews,
                Forums = forums
            };

            return moderatorSelect;
        }


        //WIP
        public async Task RemoveForumFromModeratorAsync(ClaimsPrincipal user, ForumSectionViewModel forumViewModel)
        {
            var userModel = await _userManager.FindByNameAsync(user.Identity.Name);

            if (userModel is null)
            {
                throw new KeyNotFoundException($"User was not found.");
            }

            if (!user.IsInRole("Moderator"))
            {
                throw new UnauthorizedAccessException("Not enough privileges to perform the action");
            }

            var forum = await _context.ForumSections.
                Include(x => x.ForumToModerator.Where(x => x.SectionModerator.Id == userModel.Id))
                .FirstOrDefaultAsync(x => x.Id == forumViewModel.Id);

            if (forum is null)
            {
                throw new KeyNotFoundException($"Forum was not found.");
            }

            _context.Remove(forum.ForumToModerator);
            
            await _context.SaveChangesAsync();
        }
        //WIP
        public async Task RemoveRoleFromUserAsync(ClaimsPrincipal user)
        {
            var userModel = await _userManager.FindByNameAsync(user.Identity.Name);

            if (user is null)
            {
                throw new KeyNotFoundException($"User was not found.");
            }

            var result = await _userManager.AddToRoleAsync(userModel, "ForumUser");

            if (!result.Succeeded)
            {
                throw new TaskCanceledException("Something went wrong. Please, try again");
            }

            if (user.IsInRole("Moderator"))
            {
                await _userManager.RemoveFromRoleAsync(userModel, "Moderator");
            }
        }

    }
}
