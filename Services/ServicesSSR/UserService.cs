﻿using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Backend_Task3.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<AppUserModel> _userManager;

        public UserService(UserManager<AppUserModel> userManager)
        {
            _userManager = userManager;
        }

        public Task LogInAsync()
        {
            throw new NotImplementedException();
        }

        public Task LogoutAsync()
        {
            throw new NotImplementedException();
        }

        public async Task RegisterUserAsync(UserRegisterViewModel model)
        {
            if (model.Password != model.ConfirmPassword)
            {
                throw new ArgumentException("Password and ConfirmPassword fields do not match.");
            }

            if (_userManager.Users.Any(x => x.UserName == model.UserName))
            {
                throw new Exception("A user with such name already exists.");
            }


            var user = new AppUserModel { 
                Id = Guid.NewGuid().ToString(),
                UserName = model.UserName,
            };

            var isCreated = await _userManager.CreateAsync(user, model.Password);
            await _userManager.AddToRoleAsync(user, "ForumUser");

            if (!isCreated.Succeeded)
            {
                throw new TaskCanceledException("Registration failed. Try again.");
            }
        }
    }
}
