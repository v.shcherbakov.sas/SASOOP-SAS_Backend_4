﻿using AutoMapper;
using Backend_Task2.Forum;
using Backend_Task2.Forum.Models;
using Backend_Task2.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Backend_Task3.Services
{
    public class ForumSectionService : IForumSectionService
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IMapper _mapper;

        public ForumSectionService(ApplicationDbContext context, IMapper mapper,
            IWebHostEnvironment hostEnvironment)
        {
            _context = context;
            _mapper = mapper;
            _hostEnvironment = hostEnvironment;
        }

        public async Task CreateAsync(ForumSectionCreateViewModel model, ClaimsPrincipal user)
        {

            var newForum = _mapper.Map<ForumSectionModel>(model);
            _context.Add(newForum);
            await _context.SaveChangesAsync();

        }

        public async Task DeleteAsync(ForumSectionViewModel model, ClaimsPrincipal user)
        {

            var forum = await _context.ForumSections
            .FirstOrDefaultAsync(x => x.Id == model.Id);

            if (forum is null)
            {
                throw new KeyNotFoundException("Requested forum was mot found.");
            }

            if (!(forum.Topics is null))
            {
                foreach (var topic in forum.Topics)
                {
                    if (!(topic.Messages is null))
                    {
                        foreach (var message in topic.Messages)
                        {
                            if (!(message.Attachments is null))
                            {
                                foreach (var image in message.Attachments)
                                {
                                    System.IO.File.Delete(_hostEnvironment.WebRootPath + image.FilePath);
                                }
                            }
                        }
                    }
                }
            }

            _context.Remove(forum);
            await _context.SaveChangesAsync();

        }

        public async Task<ForumSectionViewModel> DetailsAsync(int id)
        {
            var forum = await _context.ForumSections.Include(x => x.Topics).FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<ForumSectionViewModel>(forum);
        }

        public async Task EditAsync(ForumSectionEditViewModel model, ClaimsPrincipal user)
        {

            var forum = await _context.ForumSections
                .Include(x => x.ForumToModerator).ThenInclude(x => x.SectionModerator)
                .FirstOrDefaultAsync(x => x.Id == model.Id);

            if (forum is null)
            {
                throw new KeyNotFoundException();
            }

            if (user.IsInRole("Moderator") && !forum.ForumToModerator.Any(x => x.SectionModerator.UserName == user.Identity.Name))
            {
                throw new UnauthorizedAccessException("The user is not the moderator for this section.");
            }

            forum.Name = model.Name;
            forum.Description = model.Description;

            await _context.SaveChangesAsync();

        }

        public async Task<ForumSectionViewModel> GetForDeleteAsync(int id)
        {
            var forum = await _context.ForumSections.FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<ForumSectionViewModel>(forum);
        }

        public async Task<ForumSectionEditViewModel> GetForEditAsync(int id)
        {
            var forum = await _context.ForumSections.FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<ForumSectionEditViewModel>(forum);
        }

        public async Task<List<ForumSectionViewModel>> IndexAsync()
        {
            var forums = await _context.ForumSections.ToListAsync();

            return _mapper.Map<List<ForumSectionViewModel>>(forums);
        }
    }
}
